'use strict';

/* https://github.com/angular/protractor/blob/master/docs/toc.md */

describe('my app', function() {


  it('should automatically redirect to /calenderview when location hash/fragment is empty', function() {
    browser.get('index.html');
    expect(browser.getLocationAbsUrl()).toMatch("/calenderview");
  });


  describe('calenderview', function() {

    beforeEach(function() {
      browser.get('index.html#/calenderview');
    });


    it('should render calenderview when user navigates to /calenderview', function() {
      expect(element.all(by.css('[ng-view] p')).first().getText()).
        toMatch(/partial for view 1/);
    });

  });


  describe('listview', function() {

    beforeEach(function() {
      browser.get('index.html#/listview');
    });


    it('should render listview when user navigates to /listview', function() {
      expect(element.all(by.css('[ng-view] p')).first().getText()).
        toMatch(/partial for view 2/);
    });

  });
});
