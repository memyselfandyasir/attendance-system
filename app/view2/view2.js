'use strict';

angular.module('attendanceSystemApp.view2', ['ngRoute', 'attendanceSystemApp.view1'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/view2', {
            templateUrl: 'view2/view2.html',
            controller: 'ListViewCtrl'
        });
    }])

    .controller('ListViewCtrl', ['$scope', 'localStorageService', function ($scope, localStorageService) {

        $scope.gridOptions = {
            columnDefs: [
                {field: 'title', name: 'Action'},
                {field: 'start', name: 'Date', cellFilter: 'date: "fullDate"'},
                {field: 'end', name: 'Time', cellFilter: 'date: "h:mm a"'}
            ]
        };


        function initialize() {
            var eventSources = localStorageService.getState() || [];
            if (eventSources[0]) {
                _(eventSources[0]).forEach(function (event) {
                    console.log('convert', event);
                    // converting to local time zone
                    event.start = new Date(event.start);
                    event.end = new Date(event.end);
                });
                $scope.gridOptions.data = eventSources[0];
            }
        }

        initialize();
    }]);