'use strict';
/**
 * Created by yasir on 1/18/2016.
 */

angular.module('attendanceSystemApp.view1')

    .factory('localStorageService', ['$rootScope', function () {
        var __stateName = 'attendance-system-state';

        function saveState(state) {
            if (state) {
                state = JSON.stringify(state);
                localStorage.setItem(__stateName, state);
            }
        }

        function getState() {
            var state = localStorage.getItem(__stateName);
            if (state) {
                return JSON.parse(state);
            }
        }

        return {
            saveState: saveState,
            getState: getState
        };
    }]);
