'use strict';

angular.module('attendanceSystemApp.view1', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/view1', {
            templateUrl: 'view1/view1.html',
            controller: 'CalenderViewCtrl'
        });
    }])

    .controller('CalenderViewCtrl', ['$scope', '$compile', 'localStorageService', 'utilService', function ($scope, $compile, localStorageService, utilService) {
        $scope.eventSources = [[]];

        function timeIn() {
            // getting the latest entry with respect to time
            var lastEntry = _($scope.eventSources[0]).sortBy('start').value().pop();
            // only allow time in if the latest entry is not present or is of time out
            if (!lastEntry || lastEntry.title === 'Time Out') {
                $scope.eventSources[0].push({
                    title: 'Time In',
                    start: new Date(),
                    end: new Date(),
                    className: ['time-in']
                });
                localStorageService.saveState($scope.eventSources);
            } else {
                alert('Time Out first to time in again');
            }
        }

        function timeOut() {
            // getting the latest entry with respect to time
            var lastEntry = _($scope.eventSources[0]).sortBy('start').value().pop();
            // only allow time out if the latest entry is for time in
            if (!!lastEntry && lastEntry.title === 'Time In') {
                var today = new Date();
                if (lastEntry.start) {
                    // checking the last inserted date was same as the current date
                    if (!utilService.isSameDate(lastEntry.start, today)) {
                        // if this is not true that means the previous time in was some other date, first time out for
                        // that date
                        today = utilService.maxTime(lastEntry.start);
                    }
                }
                $scope.eventSources[0].push({
                    title: 'Time Out',
                    start: today,
                    end: today,
                    className: ['time-out']
                });
                localStorageService.saveState($scope.eventSources);
            } else {
                // here we can allow the user to update the time instead of showing the alert box
                alert('Time In first.');
            }
        }

        function initialize() {
            $scope.eventSources = localStorageService.getState() || $scope.eventSources;
            if ($scope.eventSources[0]) {
                _($scope.eventSources[0]).forEach(function (event) {
                    console.log('convert', event);
                    // converting to local time zone
                    event.start = new Date(event.start);
                    event.end = new Date(event.end);
                });
            }
        }

        $scope.uiConfig = {
            calendar: {
                height: 450,
                editable: true,
                header: {
                    left: 'title',
                    center: '',
                    right: 'prev,next'
                },
                eventRender: function (event, element, view) {
                    element.attr({
                        'tooltip': event.title,
                        'tooltip-append-to-body': true
                    });
                    $compile(element)($scope);
                }
            }
        };
        $scope.timeIn = timeIn;
        $scope.timeOut = timeOut;

        initialize();
    }]);