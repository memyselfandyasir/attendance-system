'use strict';

// Declare app level module which depends on views, and components
angular.module('attendanceSystemApp', [
    'ngRoute',
    'attendanceSystemApp.view1',
    'attendanceSystemApp.view2',
    'ui.calendar',
    'ui.grid',
    'attendanceSystemApp.version'
]).
    config(['$routeProvider', function ($routeProvider) {
        $routeProvider.otherwise({redirectTo: '/view1'});
    }]);
