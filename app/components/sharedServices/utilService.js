/**
 * Created by yasir on 1/23/2016.
 */
'use strict';

angular.module('attendanceSystemApp')

    .factory('utilService', ['$rootScope', function ($rootScope) {
        function isSameDate(date1, date2) {
            return withoutTime(date1) === withoutTime(date2);
        }

        /**
         * return date with 0 time
         * @param date
         * @returns {Date}
         */
        function withoutTime(date) {
            var d = new Date(date);
            d.setHours(0, 0, 0, 0);
            return d;
        }

        /**
         * returns the same date with maximum time of the day
         * @param date
         * @returns {Date}
         */
        function maxTime(date) {
            var d = new Date(date);
            d.setHours(23, 59, 59, 59);
            return d;
        }

        return {
            isSameDate: isSameDate,
            maxTime: maxTime
        };
    }]);

