'use strict';

describe('attendanceSystemApp.version module', function() {
  beforeEach(module('attendanceSystemApp.version'));

  describe('version service', function() {
    it('should return current version', inject(function(version) {
      expect(version).toEqual('0.1');
    }));
  });
});
