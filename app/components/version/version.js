'use strict';

angular.module('attendanceSystemApp.version', [
  'attendanceSystemApp.version.interpolate-filter',
  'attendanceSystemApp.version.version-directive'
])

.value('version', '0.1');
